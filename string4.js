module.exports=function getFullName(objectname){
    const result=Object.entries(objectname).reduce((previousvalue,currentvalue)=>{
        previousvalue += currentvalue[1]+""
        return previousvalue;
    },"")
    return result;
}